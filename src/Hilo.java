
import java.util.logging.Level;
import java.util.logging.Logger;


public class Hilo extends Thread{
    circulo lamina;
    boolean suspender = false;
    int n = 5;

    public void setN(int n) {
        this.n = n;
    }
    public Hilo(circulo lamina){
        this.lamina = lamina;
    }
    public Hilo(){};
    
    @Override
    public void run(){
         while(true){
            try {
                Thread.sleep(n);               
                synchronized (this) {
                    while (suspender) {
                        wait();
                    }
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(Hilo.class.getName()).log(Level.SEVERE, null, ex);
            }
            lamina.mover();
            lamina.repaint();
        }
    }
        //Suspender un hilo
    synchronized void suspenderhilo(){
        suspender=true;
    }
    //Renaudar un hilo
    synchronized void renaudarhilo(){
        suspender=false;
        notify();
    }
}

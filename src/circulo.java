
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import javax.swing.*;

public class circulo extends JPanel {

    Pelota pelota = new Pelota(this);
    Pelota pelota2 = new Pelota(this);
    Pelota pelota3 = new Pelota(this);
    Pelota pelota4 = new Pelota(this);
    Pelota pelota5 = new Pelota(this);

    raqueta raq = new raqueta(this);
    Hilo hilo;
    JLabel pun;
    int cant_esferas = 1;
    int velocidad = 5;
    int puntaje = 0;
    int i = 0;
    int j = 0;

    public int getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(int puntaje) {
        this.puntaje = puntaje;
    }

    public void setHilo(Hilo hilo) {
        this.hilo = hilo;
    }

    public int getCant_esferas() {
        return cant_esferas;
    }

    public void setCant_esferas(int cant_esferas) {
        this.cant_esferas = cant_esferas;
    }

    public int getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(int velocidad) {
        this.velocidad = velocidad;
        hilo.setN(getVelocidad());
    }

    public circulo() {

        pelota.setX(500);
        pelota.setY(300);
        pelota2.setX(600);
        pelota2.setY(400);
        pelota3.setX(400);
        pelota3.setY(200);
        pelota4.setX(700);
        pelota4.setY(300);
        pelota5.setX(300);
        pelota5.setY(300);

        addKeyListener(new KeyListener() {

            @Override
            public void keyReleased(KeyEvent e) {
                raq.keyReleased(e);
            }

            @Override
            public void keyPressed(KeyEvent e) {
                raq.keyPressed(e);
            }

            @Override
            public void keyTyped(KeyEvent ke) {
            }

        });//fin del actionListener
        puntaje = 0;
        pun = new JLabel(String.valueOf(puntaje));
        pun.setSize(250, 300);
        pun.setForeground(Color.white);
        pun.setFont(new Font("OCR A Extended", Font.BOLD, 80));

        this.setFocusable(true);
        this.hilo = hilo;
    }//fin del constructor

    public void aumentaPuntos() {
        puntaje++;
        int punt = puntaje;
        this.pun.setText(String.valueOf(punt));

    }//fin del metodo 

    public void restaPuntos() {
        puntaje--;
        int punt = puntaje;
        this.pun.setText(String.valueOf(punt));

    }//fin del metodo

    public void mover() {
        switch (getCant_esferas()) {
            case 1:
                pelota.move();
                break;
            case 2:
                pelota.move();
                pelota2.move();
                break;
            case 3:
                pelota.move();
                pelota2.move();
                pelota3.move();
                break;
            case 4:
                pelota.move();
                pelota2.move();
                pelota3.move();
                pelota4.move();
                break;
            case 5:
                pelota.move();
                pelota2.move();
                pelota3.move();
                pelota4.move();
                pelota5.move();
                break;
            default:
                break;
        }
    }

    public void lineasVerdes(Graphics g) {
        //para lineas verdes
        Graphics2D G2D = (Graphics2D) g;

        G2D.setColor(Color.green);
        G2D.setStroke(new BasicStroke(3.0f));

        QuadCurve2D N = new QuadCurve2D.Float();
        QuadCurve2D O = new QuadCurve2D.Float();
        QuadCurve2D S = new QuadCurve2D.Float();
        QuadCurve2D E = new QuadCurve2D.Float();

        //           x1,     y1,   ctrlx,   ctrly,   x2,     y2
        N.setCurve(470.0f, 060.0f, 515.0f, 053.5f, 570.0f, 60.0f);
        S.setCurve(470.0f, 588.0f, 515.0f, 600.5f, 570.0f, 588.0f);
        E.setCurve(263.0f, 280.0f, 255.0f, 350.5f, 266.0f, 380.0f);
        O.setCurve(770.0f, 280.0f, 778.0f, 340.5f, 769.0f, 380.0f);

        G2D.draw(N);
        G2D.draw(O);
        G2D.draw(S);
        G2D.draw(E);
    }//fin de lineas verdes

    public void lineasRojas(Graphics g) {
        //para lineas rojas        
        Graphics2D G2D2 = (Graphics2D) g;

        G2D2.setColor(Color.red);
        G2D2.setStroke(new BasicStroke(3.0f));

        QuadCurve2D NO = new QuadCurve2D.Float();
        QuadCurve2D SO = new QuadCurve2D.Float();
        QuadCurve2D SE = new QuadCurve2D.Float();
        QuadCurve2D NE = new QuadCurve2D.Float();

        //           x1,     y1,   ctrlx,   ctrly,   x2,     y2
        NO.setCurve(302.0f, 175.0f, 345.0f, 120.5f, 360.0f, 112.0f);
        NE.setCurve(672.0f, 112.0f, 690.0f, 120.5f, 731.0f, 175.0f);
        SO.setCurve(308.0f, 480.0f, 335.0f, 520.5f, 375.0f, 547.0f);
        SE.setCurve(726.0f, 480.0f, 698.0f, 520.5f, 668.0f, 543.0f);

        G2D2.draw(NO);
        G2D2.draw(SO);
        G2D2.draw(SE);
        G2D2.draw(NE);
    }//fin de lineas rojas

    @Override
    public void paintComponent(Graphics g) {

        this.requestFocus();
        super.paintComponent(g);
        Toolkit t = Toolkit.getDefaultToolkit();
        Image imagen = t.getImage("src//imagenes//j5.jpg");
        g.drawImage(imagen, 0, 0, this);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        if (getCant_esferas() == 1) {
            pelota.paint(g2);
        }
        if (getCant_esferas() == 2) {
            pelota.paint(g2);
            pelota2.paint(g2);
        }
        if (getCant_esferas() == 3) {
            pelota.paint(g2);
            pelota2.paint(g2);
            pelota3.paint(g2);
        }
        if (getCant_esferas() == 4) {
            pelota.paint(g2);
            pelota2.paint(g2);
            pelota3.paint(g2);
            pelota4.paint(g2);
        }
        if (getCant_esferas() == 5) {
            pelota.paint(g2);
            pelota2.paint(g2);
            pelota3.paint(g2);
            pelota4.paint(g2);
            pelota5.paint(g2);
        }
        raq.paint(g2);

        pun.setLocation(800, 30);
        this.add(pun);
        this.setVisible(true);

        //lineas verdes
        this.lineasVerdes(g);

        //lineas rojas
        this.lineasRojas(g);
    }//fin del metodo

}//fin de la clase

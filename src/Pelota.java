
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.geom.Rectangle2D;

public class Pelota {

    private int x = 500;
    private int y = 300;
    private int xa = 1, ya = 1;
    private final int DIAMETER = 30;
    private circulo game;

    public Pelota(circulo game) {
        this.game = game;
    }

    public Rectangle2D getPelota() {
        return new Rectangle2D.Double(x, y, DIAMETER, DIAMETER);
    }

    private boolean collision() {
        return game.raq.getRaqueta().intersects(getPelota());
    }

    public void paint(Graphics2D g) {
        g.setColor(Color.YELLOW);
        g.fillOval(x, y, DIAMETER, DIAMETER);
        //rayas rojas
        //NO
//        g.fillRect(290, 75, 5, 5);
//        g.fillRect(340, 170, 5, 5);
//        g.fillRect(290, 170, 5, 5);
//        g.fillRect(340, 75, 5, 5);
//        
//        //NE
//        g.fillRect(676, 90, 5, 5);
//        g.fillRect(736, 180, 5, 5);
//        g.fillRect(676, 180, 5, 5);
//        g.fillRect(736, 90, 5, 5);
//        
//        //SE
//        g.fillRect(668, 480, 5, 5);
//        g.fillRect(740, 555, 5, 5);
//        g.fillRect(668, 555, 5, 5);
//        g.fillRect(740, 480, 5, 5);
//        
//        //SO
//        g.fillRect(280, 480, 5, 5);
//        g.fillRect(375, 555, 5, 5);
//        g.fillRect(280, 555, 5, 5);
//        g.fillRect(375, 480, 5, 5);
//        
//        
//        
//        //rayas verdes
//        //norte
//        g.setColor(Color.ORANGE);
//        g.fillRect(575, 65, 5, 5);
//        g.fillRect(450, 65, 5, 5);
//        
//        //sur
//        g.fillRect(590, 580, 5, 5);
//        g.fillRect(440, 580, 5, 5);
//        
//        //este
//        g.fillRect(760, 255, 5, 5);
//        g.fillRect(760, 405, 5, 5);
//        
//        //oeste
//        g.fillRect(270, 255, 5, 5);
//        g.fillRect(270, 405, 5, 5);
//        
//        //espacios libes
//        g.setColor(Color.RED);
//        g.fillRect(450, 65, 5, 5);//arriba izquierda
//        g.fillRect(320, 65, 5, 5);
//        g.fillRect(340, 85, 5, 5);
//        g.fillRect(340, 50, 5, 5);
//        
//        g.fillRect(575, 65, 5, 5);//arriba derecha
//        g.fillRect(700, 65, 5, 5);
//        g.fillRect(680, 50, 5, 5);
//        g.fillRect(680, 110, 5, 5);
//        
//        g.fillRect(270, 160, 5, 5);//izquierda centro arriba
//        g.fillRect(270, 255, 5, 5);
//        g.fillRect(250, 177, 5, 5);
//        g.fillRect(320, 177, 5, 5);
//        
//        g.fillRect(760, 160, 5, 5);//derecha centro arriba
//        g.fillRect(760, 255, 5, 5);
//        g.fillRect(710, 170, 5, 5);
//        g.fillRect(780, 170, 5, 5);
//      
//        g.fillRect(270, 405, 5, 5);//izquierda centro abajo
//        g.fillRect(270, 480, 5, 5);
//        g.fillRect(260, 478, 5, 5);
//        g.fillRect(290, 478, 5, 5);
//        
//        g.fillRect(760, 405, 5, 5);//derecha centro abajo
//        g.fillRect(760, 490, 5, 5);
//        g.fillRect(723, 478, 5, 5);
//        g.fillRect(790, 478, 5, 5);
//        
//        g.fillRect(350, 580, 5, 5);//abajo izquierda
//        g.fillRect(440, 580, 5, 5);
//        g.fillRect(375, 555, 5, 5);
//        g.fillRect(375, 590, 5, 5);
//        
//        g.fillRect(590, 580, 5, 5);//abajo derecha
//        g.fillRect(720, 580, 5, 5);
//        g.fillRect(660, 558, 5, 5);
//        g.fillRect(720, 558, 5, 5);
//        g.fillRect(660, 550, 5, 5);
//        g.fillRect(660, 585, 5, 5);
    }

    public void move() {//NO TOCAR
        //colision con lineas verdes
        //norte       
        if ((x >= 450 && x <= 575) && (y == 65)) {
            this.game.aumentaPuntos();
            if (ya == 1) {
                ya = -1;
            } else {
                ya = 1;
            }
        }
        //sur
        if ((x >= 440 && x <= 590) && y == 580) {
            this.game.aumentaPuntos();
            if (ya == 1) {
                ya = -1;
            } else {
                ya = 1;
            }
        }
        //este
        if ((x == 760) && (y >= 255 && y <= 405)) {
            this.game.aumentaPuntos();
            if (xa == 1) {
                xa = -1;
            } else {
                xa = 1;
            }
        }
        //oeste
        if ((x == 270) && (y >= 255 && y <= 405)) {
            this.game.aumentaPuntos();
            if (xa == 1) {
                xa = -1;
            } else {
                xa = 1;
            }
        }
        //colision con lineas rojas
        //NO
        if ((x >= 290 && x <= 340) && (y >= 85 && y <= 170)) {
            xa = 1;
            if (ya == 1) {
                ya = -1;
            } else {
                ya = 1;
            }
            System.out.println("x = " + x + " y = " + y);
            this.game.restaPuntos();
        }
        //NE
        if ((x >= 676 && x <= 736) && (y >= 90 && y <= 180)) {
            this.game.restaPuntos();
            xa = -1;
            if (ya == 1) {
                ya = -1;
            } else {
                ya = 1;
            }
            System.out.println("x = " + x + " y = " + y);

        }
        //SE
        if ((x >= 668 && x <= 740) && (y >= 480 && y <= 555)) {
            this.game.restaPuntos();
            xa = -1;
            if (ya == 1) {
                ya = -1;
            } else {
                ya = 1;
            }
            System.out.println("x = " + x + " y = " + y);
        }
        //SO
        if ((x >= 270 && x <= 375) && (y >= 480 && y <= 555)) {
            this.game.restaPuntos();
            xa = 1;
            if (ya == 1) {
                ya = -1;
            } else {
                ya = 1;
            }
            System.out.println("x = " + x + " y = " + y);
        }

        //demas colisones 
        //arriba izquierda
        if ((y == 65 && (x <= 450 && x >= 320)) || (x == 340 && (y <= 85 && y >= 50))) {
            ya = -ya;
        }

        //arriba derecha
        if (((x >= 575 && x <= 700) && y == 65) || ((y >= 50 && y <= 110) && x == 680)) {
            ya = -ya;
        }
        //izquierda centro arriba
        if ((x == 270 && (y >= 160 && y <= 255)) || (y == 177 && (x >= 250 && x <= 320))) {
            xa = -xa;
        }
        //derecha centro arriba
        if ((x == 760 && (y >= 160 && y <= 255)) || (y == 170 && (x >= 710 && x <= 780))) {
            xa = -xa;
        }
        //izquierda centro abajo
        if ((x == 270 && (y >= 405 && y <= 480)) || (y == 478 && (x >= 260 && x <= 290))) {
            xa = -xa;
        }
        //derecha centro abajo
        if ((x == 760 && (y >= 405 && y <= 490)) || (y == 478 && (x >= 723 && x <= 790))) {
            xa = -xa;
        }
        //abajo izquierda
        if ((y == 580 && (x >= 350 && x <= 440)) || (x == 375 && (y >= 555 && y <= 590))) {
            ya = -ya;
        }
        //abajo derecha
        if ((y == 580 && (x >= 590 && x <= 720)) || (y == 558 && (x >= 660 && x <= 720)) || (x == 660 && (y >= 550 && y <= 585))) {
            ya = -ya;
        }
        //restricciones de emergencia
        if (x == 100) {//izquierda        
            xa = 1;
            if (ya == 1) {
                ya = -1;
            } else {
                ya = 1;
            }
        }
        if (x == 900) {//derecha
            xa = -1;
            if (ya == 1) {
                ya = -1;
            } else {
                ya = 1;
            }
        }
        if (y == 20) {//arriba  
            ya = 1;
            if (xa == 1) {
                xa = -1;
            } else {
                xa = 1;
            }
        }
        if (y == 780) {//abajo        
            ya = -1;
            if (xa == 1) {
                xa = -1;
            } else {
                xa = 1;
            }
        }
        //especificaciones para la raqueta
        if (collision()) {
            ya = -ya;
            if (ya == -1) {
                y = game.raq.getTopY() - DIAMETER;
            }
            if (ya == 1) {
                y = game.raq.getTopY() + DIAMETER;
            }
        }
        x = x + xa;
        y = y + ya;

    }//fin de move

    public int getTopY() {
        return y;
    }

    public int getLeftX() {
        return x;
    }

    public int getLowY() {
        return y;
    }

    public int getRightX() {
        return x;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
}

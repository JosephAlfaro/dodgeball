
import java.awt.*;
import java.awt.event.*;

public class raqueta {

    private static final int WIDTH = 60;
    private static final int HEIGHT = 20;
    int y = 300;
    int ya = 8;
    int x = 500;
    int xa = 8;
    private circulo game;

    public raqueta(circulo game) {
        this.game = game;
    }

    public void move() {
        if (x + xa > 0 && x + xa < game.getWidth() - WIDTH/* || (y + ya > 0 && y + ya < game.getHeight() - HEIGHT)*/) {
            x = x + xa;
            //y = y + ya;
        }
    }

    public void paint(Graphics2D g) {
        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(x, y, WIDTH, HEIGHT);
    }

    public void keyReleased(KeyEvent e) {
        x = x;
        y = y;
    }

    public void keyPressed(KeyEvent ke) {

        if (ke.isActionKey()) {
            switch (ke.getExtendedKeyCode()) {
                case KeyEvent.VK_UP:
                    if (y - ya < 120) {
                        y = y;
                    } else {
                        y -= ya;
                    }
                    break;
                case KeyEvent.VK_DOWN:
                    if (y + ya > 510) {
                        y = y;
                    } else {
                        y += ya;
                    }
                    break;
                case KeyEvent.VK_RIGHT:
                    if (x + xa > 700) {
                        x = x;
                    } else {
                        x += xa;
                    }
                    break;
                case KeyEvent.VK_LEFT:
                    if (x - xa < 300) {
                        x = x;
                    } else {
                        x -= xa;
                    }
                    break;
                default:
                    break;
            }
        }
    }

    public Rectangle getRaqueta() {
        return new Rectangle(x, y, WIDTH, HEIGHT);
    }

    public int getTopY() {
        return y;
    }

    public int getLeftX() {
        return x;
    }

    public int getLowY() {
        return y - HEIGHT;
    }

    public int getRightX() {
        return x + WIDTH;
    }
}
